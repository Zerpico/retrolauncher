﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RetroLauncher.Utils;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Controls;
using RetroLauncher.View.Wizard;

namespace RetroLauncher.ViewModel.Wizard
{
    public class MainWizardViewModel: NotifyModelBase
    {
        public MainWizardViewModel()
        {
            CurrentPage = Pages[0];
            
        }

        private ReadOnlyCollection<UserControl> _pages;
        public ReadOnlyCollection<UserControl> Pages
        {
            get
            {
                if (_pages == null)
                    CreatePages();

                return _pages;
            }
        }

        WizardThreeViewModel phaser;

        void CreatePages()
        {
            phaser = new WizardThreeViewModel();          
            phaser.OnComplete += Phaser_OnComplete;
            _pages = new List<UserControl>
                {
                    new WizardPage1() { DataContext = new WizardOneViewModel() },
                    new WizardPage2() { DataContext = new WizardTwoViewModel() },
                    new WizardPage3() { DataContext = phaser }
                }.AsReadOnly();
        }

        private void Phaser_OnComplete()
        {
            //запускаем эмулятор чтобы он создал файл настроек
            System.Diagnostics.Process emulator = new System.Diagnostics.Process();
            emulator.StartInfo.FileName = Utils.Paths.PathEmulator + "mednafen.exe";
            emulator.StartInfo.CreateNoWindow = false;
            emulator.Start();
            System.Threading.Thread.Sleep(500);
            emulator.Kill();

            isBlockUI = false;
            MovePreviousCommand.RaiseCanExecuteChanged();
            MoveNextCommand.RaiseCanExecuteChanged();
            Utils.Storage.SetValue("BaseLoad", true);
            Utils.Storage.Save();

        }

       

        public string Title
        {
            get { return CurrentPage == null ? string.Empty : (CurrentPage.DataContext as WizardBaseViewModel).Title; }
        }

        int CurrentPageIndex
        {
            get
            {

                if (CurrentPage == null)
                {
                    Debug.Fail("Why is the current page null?");
                }
               
                
                return Pages.IndexOf(CurrentPage);
            }
        }

       

        private UserControl _currentPage;
        public UserControl CurrentPage
        {
            get { return _currentPage; }
            private set
            {
                if (value == _currentPage)
                    return;

                if (_currentPage != null)
                    (_currentPage.DataContext as WizardBaseViewModel).IsCurrentPage = false;

                _currentPage = value;

                if (_currentPage != null)
                    (_currentPage.DataContext as WizardBaseViewModel).IsCurrentPage = true;


                OnPropertyChanged("CurrentPage");


               
                    
               
                MovePreviousCommand.RaiseCanExecuteChanged();
                MoveNextCommand.RaiseCanExecuteChanged();
                OnPropertyChanged("Title");
                OnPropertyChanged("CurrentPage");
                OnPropertyChanged("IsOnLastPage");

                
            }
        }

        public bool IsOnLastPage
        {
            get { return CurrentPageIndex == Pages.Count - 1; }
        }

        bool isBlockUI = false;

        #region Commands

        private DelegateCommand _moveNextCommand;
        public DelegateCommand MoveNextCommand
        {
            get
            {
                return _moveNextCommand ?? (_moveNextCommand = new DelegateCommand(
                                                                   arg => MoveToNextPage(),
                                                                   arg => CanMoveToNextPage));
            }
        }               

        bool CanMoveToNextPage
        {
            get { return isBlockUI ? false : (CurrentPage != null && (CurrentPage.DataContext as WizardBaseViewModel).IsValid()); }
        }

        void MoveToNextPage()
        {
            if (CanMoveToNextPage)
            {
                if (CurrentPageIndex < Pages.Count - 1)
                {
                    CurrentPage = Pages[CurrentPageIndex + 1];
                    if (CurrentPageIndex == 2)
                    {
                        if (!phaser.IsFinish)
                        {
                            isBlockUI = true;
                            MovePreviousCommand.RaiseCanExecuteChanged();
                            MoveNextCommand.RaiseCanExecuteChanged();
                            (CurrentPage.DataContext as WizardThreeViewModel).Start();
                        }
                    }
                }
                else
                    OnRequestClose();
            }
        }

        private DelegateCommand _movePreviousCommand;
        public DelegateCommand MovePreviousCommand
        {
            get
            {
                return _movePreviousCommand ?? (_movePreviousCommand = new DelegateCommand(
                                                                           args => MoveToPreviousPage(),
                                                                           args => CanMoveToPreviousPage));
            }
        }

        bool CanMoveToPreviousPage
        {
            get { return isBlockUI ? false : (0 < CurrentPageIndex); }
        }

        void MoveToPreviousPage()
        {
            if (CanMoveToPreviousPage)
                CurrentPage = Pages[CurrentPageIndex - 1];
        }


        private DelegateCommand _cancelCommand;
        public DelegateCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                    _cancelCommand = new DelegateCommand(arg => CancelOrder());

                return _cancelCommand;
            }
        }

        void CancelOrder()
        {
            OnRequestClose();
        }

        #endregion

        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            EventHandler handler = RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
