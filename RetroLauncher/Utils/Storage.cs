﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RetroLauncher.Utils
{
    public class Storage
    {        
        static Dictionary<string, object> items;

        public static object GetValue(string name)
        {
            if (items.ContainsKey(name))
                return items[name];
            return null;
        }

        public static void SetValue(string name, object value)
        {
            if (items.ContainsKey(name))
                items[name] = value;
            else
                items.Add(name, value);
        }

        public static bool IsNotSettings()
        {
            if (Directory.Exists(Paths.PathApp))
                return true;
            return false;
        }

        public static void Load()
        {
            if (!File.Exists(Paths.PathSettingsApp))
                if (!Create())
                    throw new System.IO.IOException("Не удалось создать файл: " + Paths.PathSettingsApp);

         
                //загружаем элементы
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Paths.PathSettingsApp);

                // получим корневой элемент
                XmlElement xRoot = xmlDoc.DocumentElement;

                items = new Dictionary<string, object>();
                foreach (XmlNode item in xRoot.ChildNodes)
                {
                    double tryDouble;
                    int tryInt;
                    bool trybool;

                    if (int.TryParse(item.InnerText, out tryInt))
                        items.Add(item.Name, tryInt);
                    else if (double.TryParse(item.InnerText, out tryDouble))
                        items.Add(item.Name, tryDouble);
                    else if (bool.TryParse(item.InnerText, out trybool))
                        items.Add(item.Name, trybool);
                    else items.Add(item.Name, item.InnerText);

                }

            
           
        }



        private static bool Create()
        {
            try
            {
                //загружаем файл
                XmlDocument doc = new XmlDocument();
                if (!Directory.Exists(Paths.PathApp))
                    Directory.CreateDirectory(Paths.PathApp);
                if (!File.Exists(Paths.PathSettingsApp))
                {
                    XmlTextWriter textWritter = new XmlTextWriter(Paths.PathSettingsApp, null);
                    textWritter.WriteStartDocument();
                    textWritter.WriteStartElement("settings");
                    textWritter.WriteEndElement();
                    textWritter.Close();
                }

                //Создаем элементы
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Paths.PathSettingsApp);

                items = new Dictionary<string, object>();
                items.Add("ProxyType", "None");
                items.Add("ProxyHost", "");
                items.Add("ProxyPort", 0);
                items.Add("WindowWidth", 0);
                items.Add("WindowHeight", 0);
                items.Add("BaseLoad", false);

                foreach (var item in items)
                {
                    XmlElement subRoot = xmlDoc.CreateElement(item.Key);
                    subRoot.InnerText = item.Value.ToString();
                    xmlDoc.DocumentElement.AppendChild(subRoot);
                }

                xmlDoc.Save(Paths.PathSettingsApp);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static void Save()
        {
            try
            {
                //загружаем файл
                XmlDocument doc = new XmlDocument();
                if (!Directory.Exists(Paths.PathApp))
                    Directory.CreateDirectory(Paths.PathApp);
                // if (!File.Exists(AppDataPath + "\\KadryNet\\settings.xml"))
                // {
                XmlTextWriter textWritter = new XmlTextWriter(Paths.PathSettingsApp, null);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("settings");
                textWritter.WriteEndElement();
                textWritter.Close();
                //  }

                //Создаем элементы
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Paths.PathSettingsApp);

                XmlElement xRoot = xmlDoc.CreateElement("settings");
                foreach (var item in items)
                {
                    XmlElement subRoot = xmlDoc.CreateElement(item.Key);
                    subRoot.InnerText = item.Value.ToString();
                    xmlDoc.DocumentElement.AppendChild(subRoot);
                }

                xmlDoc.Save(Paths.PathSettingsApp);

            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Не удалось сохранить настройки приложения.\nНо вы не расстраивайтесь, это не критично!", "Ошибка!", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }

        }
    }
}
