﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetroLauncher.ViewModel.Wizard
{
    class WizardTwoViewModel : WizardBaseViewModel
    {
        public override string Title
        {
            get
            {
                return "Ознакомление";
            }
        }

        public override bool IsValid()
        {
            return true;
        }
    }
}
