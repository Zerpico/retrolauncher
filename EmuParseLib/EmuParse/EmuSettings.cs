﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmuParseLib.EmuParse
{
    /// <summary>
    /// Настройки парсера сайта эмулятора
    /// </summary>
    public class EmuSettings : IParserSettings
    {
        public string BaseUrl { get; set; } = @"http://mednafen.github.io/";
        public string[] Prefix { get; set; } = { "releases/" };
    }
}
