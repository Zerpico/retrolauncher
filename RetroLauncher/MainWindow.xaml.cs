﻿using RetroLauncher.Data;
using RetroLauncher.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RetroLauncher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      
        public MainWindow()
        {
            //Инициализация настроек
            if (Utils.Storage.IsNotSettings())
            {
                Utils.Storage.Load();
                if ((bool)Utils.Storage.GetValue("BaseLoad") == false)
                    LoadSettings();
            }
            else
                LoadSettings();

            

            InitializeComponent();


            DataContext = new ViewModel.GameBaseViewModel(new GameService());


        }

        void LoadSettings()
        {            
            var mainWindow = new View.Wizard.WizardView();
            var viewModel = new ViewModel.Wizard.MainWizardViewModel();
            viewModel.RequestClose += (sender, args) => mainWindow.Close();
            mainWindow.DataContext = viewModel;
            mainWindow.ShowDialog();
        }

        private void CheckBox_filter_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as ViewModel.GameBaseViewModel).Refresh();
        }
    }

    
}
