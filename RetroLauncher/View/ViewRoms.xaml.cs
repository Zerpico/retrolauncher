﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RetroLauncher.View
{
    /// <summary>
    /// Логика взаимодействия для ViewRoms.xaml
    /// </summary>
    public partial class ViewRoms : Window
    {
        public event Action<string> OnSelect;
        string DirPath;

        public ViewRoms(string RomsPath)
        {
            InitializeComponent();
            DirPath = RomsPath;

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(DirPath);
            var files = dir.GetFiles().Where(dd => dd.Name.Contains("Hack") == false);
           
            listBox_games.ItemsSource = files.Select(dd => dd.Name); //System.IO.Directory.GetFiles(RomsPath);

            this.Loaded += ViewRoms_Loaded;
        }

        private void ViewRoms_Loaded(object sender, RoutedEventArgs e)
        {
            this.Deactivated += (send, ev) =>
            {
                this.Close();
            };
        }

        private void ListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (listBox_games.SelectedItem != null)
            {
                OnSelect?.Invoke(DirPath +"\\"+ listBox_games.SelectedItem.ToString());
            }
        }
    }
}
