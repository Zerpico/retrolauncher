﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RetroLauncher.View
{
    /// <summary>
    /// Логика взаимодействия для GameListControl.xaml
    /// </summary>
    public partial class GameListControl : UserControl
    {
        public GameListControl()
        {
            InitializeComponent();
        }

        private void GameList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //велик конечно, но я не придумал другого
            (DataContext as ViewModel.GameBaseViewModel).ShowDetailPanel();
        }
    }
}
