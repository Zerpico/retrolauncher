﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RetroLauncher.Data;
using RetroLauncher.Utils;

namespace RetroLauncher.Model
{
    public class GameService : IGameService
    {
        private ObservableCollection<IGame> _games;
        Utils.LoaderService downloader;

        //евенты ддля загрузки игры
        public event Action<int> OnProgressDownload;
        public event Action OnComplete;

        public GameService()
        {
            _games = new ObservableCollection<IGame>();
            _games = new ObservableCollection<IGame>(FBClient.GetBase(100, 0));            
        }

        //а надо ли мне это?
        public IGame[] FindGame(IGame findBook)
        {
            throw new NotImplementedException();
        }

        //поиск игр по фильтрам
        public (int,IGame[]) FindGame(int count, int skipCount, string name, string[] platform, string[] genre)
        {
            return FBClient.FindGame(count, skipCount, name, platform, genre);
        }


        public void GetData(Action<ObservableCollection<IGame>, Exception> callback)
        {
            callback(_games, null);
        }

        //сохранить в базу что игру скачали
        public void SetDownloaded(IGame game)
        {
            FBClient.SetDownloaded(game);
        }

        string lastGamePath;

        public void DownloadGame(IGame game)
        {
            //создаем пути если их нет
            if (!System.IO.Directory.Exists(Paths.PathGames))
                System.IO.Directory.CreateDirectory(Paths.PathGames);

            if (!System.IO.Directory.Exists(Paths.PathGames + game.Platform))
                System.IO.Directory.CreateDirectory(Paths.PathGames + game.Platform);

            //настраиваем парсер для выдирания ссылки на архив с ромами игры с сайта
            lastGamePath = Paths.PathGames + game.Platform + "\\" + game.Name + ".7z";
            var parseSet = new EmuParseLib.UrlParse.UrlSettings(game.Url);

            var parser = new EmuParseLib.ParserWorker<string>(
                new EmuParseLib.UrlParse.UrlParse(), parseSet);

            //запускаем парсер
            parser.OnNewData += Parser_OnNewData;
            parser.Start();

        }


        private void Parser_OnNewData(object arg1, string arg2)
        {            
            //нашли ссылку, теперь скачиваем её в потоке с подписыванием на прогресс
            downloader = new LoaderService();
            downloader.OnProgress += Downloader_OnProgress;
            downloader.OnComplete += Downloader_OnComplete;
            downloader.DownloadFile(arg2, lastGamePath);
        }

        private void Downloader_OnComplete(bool obj)
        {
            //отписываемся от событий
            downloader.OnProgress -= Downloader_OnProgress;
            downloader.OnComplete -= Downloader_OnComplete;
            downloader = null;

            //извелкаем архив
            LoaderService.ExtractSevenZip(lastGamePath, lastGamePath.Substring(0, lastGamePath.Length-3));
            System.IO.File.Delete(lastGamePath);
            lastGamePath = null;

            //уведомляем что всё скачали
            OnComplete?.Invoke();
        }

        private void Downloader_OnProgress(long arg1, long arg2, int arg3)
        {
            //шаг прогресса скачивания
            OnProgressDownload?.Invoke(arg3);
        }

        //запуск выбранной игры
        public void StartGame(IGame game)
        {
            var launchGame = game;
            //создаем окно для выбора рома из папки
            var viewRoms = new View.ViewRoms(Paths.PathGames + launchGame.Platform + "\\" + launchGame.Name);
           

            viewRoms.Show();            
            viewRoms.OnSelect += ViewRoms_OnSelect;           

        }

        //запускаем выбраную игру
        private void ViewRoms_OnSelect(string obj)
        {
            Process.Start("\"" + Paths.PathEmulator + "\\mednafen.exe\"", "\"" + obj + "\"");
        }

       
    }
}
