﻿using RetroLauncher.Data;
using RetroLauncher.Model;
using RetroLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RetroLauncher.ViewModel
{
    public class GameBaseViewModel : NotifyModelBase
    {
        private readonly IGameService _gameService;

        //общее количество найденых игр
        int countGames;
        //количество страниц из наденых игр (всегла по 100 элементов на страницу)
        public int Pages { get => countGames / 100 < 1 ? 1 : countGames / 100; }
        //текущая страница
        int currentPage = 1;
        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; Refresh(); }
        }
        
        public GameBaseViewModel(IGameService gameService)
        {            
            _gameService = gameService;
            _gameService.GetData(
                (items, error) =>
                {
                    Games = items;
                    var f = error;
                });

            //получаем список скачаных игр
            DownloadedGames = new ObservableCollection<IGame>(FBClient.GetBaseDownloaded());
            
            //список жанров
            var genres = new ObservableCollection<string>(FBClient.GetGenre());
            GenreList = new ObservableCollection<CheckBoxListItem>();
            foreach (var genre in genres)
            {
                GenreList.Add(new CheckBoxListItem(false, genre));
            }

            //список платформ
            var platforms = new ObservableCollection<string>(FBClient.GetPlatform());
            PlatformList = new ObservableCollection<CheckBoxListItem>();
            foreach (var platform in platforms)
            {
                PlatformList.Add(new CheckBoxListItem(false, platform));
            }

            countGames = FBClient.GetCountGames();
            OnPropertyChanged("UserDataSize");
            BlockPanel = true;

            HideDetailPanel();           
        }

        //z-index детальной панели
        public int ZindexDetal { get; set; }
        
        //спрятать панель детальной информации
        public void HideDetailPanel()
        {
            BlockPanel = true;
            OnPropertyChanged("BlockPanel");
            PopupBlinking = false;
            OnPropertyChanged("PopupBlinking");
            ZindexDetal = 0;
            OnPropertyChanged("ZindexDetal");
        }

        //показать панель детальной информации
        public void ShowDetailPanel()
        {
            BlockPanel = false; OnPropertyChanged("BlockPanel");
            PopupBlinking = true;
            OnPropertyChanged("PopupBlinking");
            ZindexDetal = 2;
            OnPropertyChanged("ZindexDetal");
        }

       
        //для блокирвки UI
        public bool BlockPanel { get; set; }

        //если что-то выбрали в фильтре жанров
        public bool IsGenreCheck
        {
            get { return GenreList.Where(dd => dd.Checked == true).Count() > 0; }
        }

        //если что-то выбрали в фильтре платформ
        public bool IsPlatformCheck
        {
            get { return PlatformList.Where(dd => dd.Checked == true).Count() > 0; }
        }

        //если поле поиска имени не пустое
        public bool IsNameCheck
        {
            get { return SearchText == null ? false : SearchText.Length > 0; }
        }

        //справочник платформ и жанров
        public ObservableCollection<CheckBoxListItem> PlatformList { get; set; }
        public ObservableCollection<CheckBoxListItem> GenreList { get; set; }
        
        //список игр и скачаных игр
        public ObservableCollection<IGame> DownloadedGames { get; set; }
        public ObservableCollection<IGame> Games { get; set; }

        //выбранная текущая игра
        private IGame selectedGame;
        public IGame SelectedGame
        {
            get { return selectedGame; }
            set
            {
                selectedGame = value;
                OnPropertyChanged("SelectedGame");
            }
        }

        //для анимации отображения панелей
        public bool PopupBlinking { get; set; }      

        //выбраная игра из скачаных
        private IGame selectedDownloadedGame;
        public IGame SelectedDownloadedGame
        {
            get { return selectedDownloadedGame; }
            set
            {
                selectedDownloadedGame = value;              
                _gameService.StartGame(selectedDownloadedGame);

                selectedDownloadedGame = null;
                OnPropertyChanged("SelectedDownloadedGame");
            }
        }

        //определить размер занимаемого места на диске пользовательских данных
        public string UserDataSize
        {
            get
            {
                var res = "";
                Task.Factory.StartNew(() =>
                {
                    DirectoryInfo di = new DirectoryInfo(Paths.PathApp);
                    var sizes = di.EnumerateFiles("*", SearchOption.AllDirectories).Sum(fi => fi.Length);
                    
                    if (sizes > 1024 && sizes < 1024 * 1024)
                        res = (sizes / 1024).ToString() + " Kb";
                    if (sizes > 1024 * 1024)
                        res = (sizes / 1024 / 1024).ToString() + " Mb";
                }).Wait();
                return res;
            }
        }

        //поиск игры по названию
        private string searchText { get; set; }
        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
              
                    Refresh();
                
                OnPropertyChanged("SearchText");
                OnPropertyChanged("IsNameCheck");
            }
        }

        //обновление фильтра
        public void Refresh()
        {
            OnPropertyChanged("IsGenreCheck");
            OnPropertyChanged("IsPlatformCheck");

            //отбираем список выбраных платформ и жанров
            var selPlatform = PlatformList.Where(dd => dd.Checked == true).Select(vv => vv.Text).ToArray();
            var selGenre = GenreList.Where(dd => dd.Checked == true).Select(vv => vv.Text).ToArray();

            //забираем массив найденых игр
            var result = _gameService.FindGame(currentPage * 100, (currentPage - 1) * 100, SearchText, selPlatform, selGenre);
            Games = new ObservableCollection<IGame>( result.Item2);
            countGames = result.Item1;
            //запускаем уведомления об обновлении UI
            OnPropertyChanged("CurrentPage");
            OnPropertyChanged("Pages");
            OnPropertyChanged("Games");
        }

        //при смене страницы
        void ChangePage()
        {
            Refresh();
        }
       

        

        #region Commands

        //переход на следующую страницу
        private DelegateCommand nextPageCommand;
        public DelegateCommand NextPageCommand
        {
            get
            {
                return nextPageCommand ?? (nextPageCommand = new DelegateCommand(
                                                                   arg => MoveToNextPage(),
                                                                   arg => CanMoveToNextPage));
            }
        }

        
        bool CanMoveToNextPage
        {   //блокировка кнопки если текущая страница последняя
            get { return currentPage < Pages;  }
        }

        void MoveToNextPage()
        {
            if (CanMoveToNextPage)
            {
                CurrentPage++;
                PrevPageCommand.RaiseCanExecuteChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        //переход на предыдущую страницу
        private DelegateCommand prevPageCommand;
        public DelegateCommand PrevPageCommand
        {
            get
            {
                return prevPageCommand ?? (prevPageCommand = new DelegateCommand(
                                                                   arg => MoveToPrevPage(),
                                                                   arg => CanMoveToPrevPage));
            }
        }

        bool CanMoveToPrevPage
        {
            get { return currentPage > 1; }
        }

        void MoveToPrevPage()
        {
            if (CanMoveToPrevPage)
            {
                CurrentPage--;
                PrevPageCommand.RaiseCanExecuteChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        int progressDownload { get; set; }
        public int ProgressDownload
        {
            get { return progressDownload; }
            set { progressDownload = value; OnPropertyChanged("ProgressDownload"); }
        }

        //кнопка для скачивания или запуска игры
        private DelegateCommand downloadCommand;
        public DelegateCommand DownloadCommand
        {
            get
            {
                return downloadCommand ?? (downloadCommand = new DelegateCommand(obj =>
                {
                    if (obj != null)
                        SelectedGame = (IGame)obj;
                    if (!SelectedGame.Downloaded) // если не скачана
                    {
                        //скачиваем игру и подписываемся на прогресс и завершение скачивания
                        BlockPanel = false; OnPropertyChanged("BlockPanel");
                        (SelectedGame as Game).ProgressDownload = 5;
                        _gameService.DownloadGame(SelectedGame);
                        _gameService.OnProgressDownload += _gameService_OnProgressDownload;
                        _gameService.OnComplete += _gameService_OnComplete;
                    }
                    else
                    {   //иначе уже запускаем игру
                        _gameService.StartGame(SelectedGame);
                    }
                },
                (obj) => true));
            }
        }

        //при скачивании игры
        private void _gameService_OnComplete()
        {
            //отписываемся от евентов
            _gameService.OnProgressDownload -= _gameService_OnProgressDownload;
            _gameService.OnComplete -= _gameService_OnComplete;
            (SelectedGame as Game).ProgressDownload = 0;

            //сохрарняем 
            SelectedGame.Downloaded = true;
            DownloadedGames.Add(SelectedGame);
            _gameService.SetDownloaded(SelectedGame);
         
            //обновляем UI
            BlockPanel = true;
            OnPropertyChanged("BlockPanel");
            OnPropertyChanged("DownloadedGames");
            OnPropertyChanged("UserDataSize");
        }

        //Обновляем прогресс скачивания игры
        private void _gameService_OnProgressDownload(int obj)
        {
            (SelectedGame as Game).ProgressDownload = obj;
        }

       
        #endregion

    }

    //класс для CheckedListBox 
    public class CheckBoxListItem
    {
        public bool Checked { get; set; }
        public string Text { get; set; }

        public CheckBoxListItem(bool ch, string text)
        {
            Checked = ch;
            Text = text;
        }
    }
}
