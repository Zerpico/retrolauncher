﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirebirdSql.Data.FirebirdClient;
using RetroLauncher.Data;

namespace RetroLauncher.Utils
{
    class FBClient
    {
        static string ConnectionString
        {
            get
            {      
                string conString =  $"User=SYSDBA;Password=masterkey;Database={Paths.PathApp}\\GAMEBASELIBRARY.GDB;DataSource=localhost;" +
                "Port = 3050; Dialect = 3; Charset = UTF8; Role =; Connection lifetime = 15; Pooling = true;" +
                "MinPoolSize = 0; MaxPoolSize = 50; Packet Size = 8192; ServerType = 1; client library=client\\fbembed.dll";


                return conString;
            }
        }

        public static void CreateDatabase( )
        {
            FbConnection.CreateDatabase(ConnectionString);

            FbConnection con = new FbConnection(ConnectionString);
            con.Open();

            //создаем генератор
            FbTransaction fbTransaction = con.BeginTransaction("InsertTable");
            FbCommand com = new FbCommand("CREATE GENERATOR \"GEN_games_ID\";", con, fbTransaction);
            com.ExecuteNonQuery();

            //создаем таблицу
            com.CommandText = "CREATE TABLE GAMES (\n" +
                "ID          INTEGER NOT NULL,\n" +
                "PLATFORM    VARCHAR(50) NOT NULL,\n" +
                "NAME        VARCHAR(100) NOT NULL,\n" +
                "SECONDNAME  VARCHAR(100),\n" +
                "OTHERNAME   VARCHAR(100),\n" +
                "YEARS        INTEGER DEFAULT null,\n" +
                "DEVELOPER   VARCHAR(100),\n" +
                "PLAYERS     INTEGER DEFAULT null,\n" +
                "GENRE       VARCHAR(100),\n" +
                "ANNOTATION  VARCHAR(1000),\n" +
                "URL         VARCHAR(255) NOT NULL,\n" +
                "IMGURL     VARCHAR(255),\n" +
                "DOWNLOADED SMALLINT DEFAULT 0\n" +
            "); ";
            com.ExecuteNonQuery();

            //создаем первый ключ
            com.CommandText = "ALTER TABLE GAMES ADD PRIMARY KEY(ID)";
            com.ExecuteNonQuery();

            //создаем триггер
            com.CommandText = "CREATE OR ALTER TRIGGER GAMES_BI0 FOR GAMES\n"+
                "ACTIVE BEFORE INSERT POSITION 0\n"+
                "AS\n"+
                "begin\n"+
                "  if (new.ID is null) then\n"+
                "  new.ID = gen_id(\"GEN_games_ID\", 1);\n"+
                "            end";
            com.ExecuteNonQuery();

            //создаем первый ключ
           // com.CommandText = "GRANT ALL ON GAMES TO PUBLIC;";
           // com.ExecuteNonQuery();

            fbTransaction.Commit();
            con.Close();
        }

        public static IEnumerable<string> GetGenre()
        {
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
            List <string> result = new List<string>();
            string sql = "SELECT distinct(GENRE) From GAMES";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;
          

            FbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {               
                var genre = reader["Genre"] == DBNull.Value ? string.Empty : reader["Genre"].ToString();  
                result.Add(genre);
            } 

            con.Close();

            return result;
        }

        public static IEnumerable<string> GetPlatform()
        {
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
            List<string> result = new List<string>();
            string sql = "SELECT distinct(PLATFORM) From GAMES";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;


            FbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var platform = reader["Platform"] == DBNull.Value ? string.Empty : reader["Platform"].ToString();
                result.Add(platform);
            }

            con.Close();

            return result;
        }

        public static void SetDownloaded(IGame game)
        {
            FbConnection con = new FbConnection(ConnectionString);
            string sql = "UPDATE GAMES SET \"DOWNLOADED\" = 1 WHERE ID = " + game.Id + " ";

            con.Open();
            FbTransaction fbTransaction = con.BeginTransaction("InsertGame");
            FbCommand cmd = new FbCommand(sql, con, fbTransaction);

            cmd.CommandType = CommandType.Text;
            FbDataReader reader = cmd.ExecuteReader();
            fbTransaction.Commit();
            con.Close();
        }

        public static IEnumerable<IGame> GetBaseDownloaded()
        {
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
            List <Game> result = new List<Game>();
            string sql = "SELECT * FROM GAMES WHERE \"DOWNLOADED\" = 1";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;
          

            FbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Game newGame = new Game();
                newGame.Id = int.Parse(reader["Id"].ToString());
                newGame.Platform = reader["Platform"].ToString();
                newGame.Name = reader["Name"].ToString();
                newGame.SecondName = reader["SecondName"].ToString();
                newGame.OtherName = reader["OtherName"].ToString();
                newGame.Year = reader["Years"] == DBNull.Value? new Nullable<int>() : int.Parse(reader["Years"].ToString());
                newGame.OtherName = reader["Developer"] == DBNull.Value ? string.Empty : reader["Developer"].ToString();
                newGame.Players = reader["Players"] == DBNull.Value ? new Nullable<int>() : int.Parse(reader["Players"].ToString());
                newGame.Genre = reader["Genre"] == DBNull.Value ? string.Empty : reader["Genre"].ToString();
                newGame.Annotation = reader["Annotation"] == DBNull.Value ? string.Empty : reader["Annotation"].ToString();
                newGame.Url = reader["Url"] == DBNull.Value ? string.Empty : reader["Url"].ToString();
                newGame.ImgUrl = reader["ImgUrl"] == DBNull.Value ? string.Empty : reader["ImgUrl"].ToString();
                newGame.Downloaded = (short)reader["DOWNLOADED"] == 0 ? false : true;

                result.Add(newGame);
            } 

            con.Close();

            return result;
        }

        public static IEnumerable<IGame> GetBase(int Count, int SkipCount)
        {
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
            List<Game> result = new List<Game>();
            string sql = "SELECT FIRST "+Count+" SKIP "+SkipCount+" * FROM GAMES";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;


            FbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Game newGame = new Game();
                newGame.Id = int.Parse(reader["Id"].ToString());
                newGame.Platform = reader["Platform"].ToString();
                newGame.Name = reader["Name"].ToString();
                newGame.SecondName = reader["SecondName"].ToString();
                newGame.OtherName = reader["OtherName"].ToString();
                newGame.Year = reader["Years"] == DBNull.Value ? new Nullable<int>() : int.Parse(reader["Years"].ToString());
                newGame.OtherName = reader["Developer"] == DBNull.Value ? string.Empty : reader["Developer"].ToString();
                newGame.Players = reader["Players"] == DBNull.Value ? new Nullable<int>() : int.Parse(reader["Players"].ToString());
                newGame.Genre = reader["Genre"] == DBNull.Value ? string.Empty : reader["Genre"].ToString();
                newGame.Annotation = reader["Annotation"] == DBNull.Value ? string.Empty : reader["Annotation"].ToString();
                newGame.Url = reader["Url"] == DBNull.Value ? string.Empty : reader["Url"].ToString();
                newGame.ImgUrl = reader["ImgUrl"] == DBNull.Value ? string.Empty : reader["ImgUrl"].ToString();
                newGame.Downloaded = (short)reader["DOWNLOADED"] == 0 ? false : true;
             
                result.Add(newGame);
            }

            con.Close();

            return result;
        }


        public static (int, IGame[]) FindGame(int Count, int SkipCount, string name, string[] platform, string[] genre)
        {
            if (name != null)
            {
                name = name.ToLower();
                if (name.Contains("'"))
                    name = name.Replace("'", "");
            }
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
            List<Game> result = new List<Game>();
            string addSql = string.Empty;
            string sql = "SELECT FIRST " + Count + " SKIP " + SkipCount + " * FROM GAMES\n";
            if (name != null && name.Length > 0)
                addSql = "((LOWER(NAME) like '%" + name+ "%') or (LOWER(SECONDNAME) like '%" + name + "%') or (LOWER(OTHERNAME) like '%" + name + "%'))";
                        
            if (genre.Length > 0)
            {
                if (!string.IsNullOrEmpty(addSql))
                    addSql += " AND (";
                else addSql += " (";
                for (int i=0;i < genre.Length; i++)
                {
                    if (i > 0 && i < genre.Length)
                        addSql += " OR GENRE = '" + genre[i]+"'";
                    if (i == 0)
                        addSql += "GENRE = '" + genre[i]+"'";
                }
                addSql += " )";
            }

            if (platform.Length > 0)
            {
                if (!string.IsNullOrEmpty(addSql))
                    addSql += " AND (";
                else addSql += " (";
                for (int i = 0; i < platform.Length; i++)
                {
                    if (i > 0 && i < platform.Length)
                        addSql += " OR PLATFORM = '" + platform[i] + "'";
                    if (i == 0)
                        addSql += "PLATFORM = '" + platform[i] + "'";
                }
                addSql += " )";
            }

            if (!string.IsNullOrEmpty(addSql))
                sql += "WHERE " + addSql;

            sql += "\nORDER BY NAME";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;


            FbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Game newGame = new Game();
                newGame.Id = int.Parse(reader["Id"].ToString());
                newGame.Platform = reader["Platform"].ToString();
                newGame.Name = reader["Name"].ToString();
                newGame.SecondName = reader["Name"].ToString();
                newGame.OtherName = reader["Name"].ToString();
                newGame.Year = reader["Years"] == DBNull.Value ? new Nullable<int>() : int.Parse(reader["Years"].ToString());
                newGame.Developer = reader["Developer"] == DBNull.Value ? string.Empty : reader["Developer"].ToString();
                newGame.Players = reader["Players"] == DBNull.Value ? new Nullable<int>() : int.Parse(reader["Players"].ToString());
                newGame.Genre = reader["Genre"] == DBNull.Value ? string.Empty : reader["Genre"].ToString();
                newGame.Annotation = reader["Annotation"] == DBNull.Value ? string.Empty : reader["Annotation"].ToString();
                newGame.Url = reader["Url"] == DBNull.Value ? string.Empty : reader["Url"].ToString();
                newGame.ImgUrl = reader["ImgUrl"] == DBNull.Value ? string.Empty : reader["ImgUrl"].ToString();
                newGame.Downloaded = (short)reader["DOWNLOADED"] == 0 ? false : true;

                result.Add(newGame);
            }

            cmd.CommandText = "SELECT COUNT(*) FROM GAMES\n";
            if (!string.IsNullOrEmpty(addSql))
                cmd.CommandText += "WHERE " + addSql;
            int countResult = (int)cmd.ExecuteScalar();

            con.Close();

            return (countResult, result.ToArray());
        }


        public static int GetCountGames()
        {
            FbConnection con = new FbConnection(ConnectionString);
            con.Open();
          
            string sql = "SELECT COUNT(*) FROM GAMES";
            FbCommand cmd = new FbCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            int result = (int)cmd.ExecuteScalar();
           

            return result;
        }

        public static void Insert(IEnumerable<IGame> lists)
        {
            FbConnection con = new FbConnection(ConnectionString);
            string sql = "INSERT INTO GAMES ( PLATFORM, NAME, SECONDNAME, OTHERNAME, YEARS, DEVELOPER, PLAYERS, GENRE, ANNOTATION, URL, IMGURL, DOWNLOADED ) \n" +
                        "VALUES (@Platform, @Name, @SecondName, @OtherName, @Years, @Developer, @Players, @Genre, @Annotation, @Url, @ImgUrl, @DOWNLOADED)";
            
            con.Open();
            FbTransaction fbTransaction = con.BeginTransaction("InsertGame");
            FbCommand cmd = new FbCommand(sql, con, fbTransaction);

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@Platform", FbDbType.VarChar);
            cmd.Parameters.Add("@Name", FbDbType.VarChar);
            cmd.Parameters.Add("@SecondName", FbDbType.VarChar);
            cmd.Parameters.Add("@OtherName", FbDbType.VarChar);
            cmd.Parameters.Add("@Years", FbDbType.Integer);
            cmd.Parameters.Add("@Developer", FbDbType.VarChar);
            cmd.Parameters.Add("@Players", FbDbType.Integer);
            cmd.Parameters.Add("@Genre", FbDbType.VarChar);
            cmd.Parameters.Add("@Annotation", FbDbType.VarChar);
            cmd.Parameters.Add("@Url", FbDbType.VarChar);
            cmd.Parameters.Add("@ImgUrl", FbDbType.VarChar);
            cmd.Parameters.Add("@DOWNLOADED", FbDbType.SmallInt);


            foreach (var item in lists)
            {
                cmd.Parameters["Platform"].Value = item.Platform;
                cmd.Parameters["Name"].Value = item.Name.Replace("'","").Replace("\"","");

                if (item.SecondName != null)
                    item.SecondName = item.SecondName.Replace("'", "").Replace("\"", "");
                cmd.Parameters["SecondName"].Value = item.SecondName;

                if (item.OtherName != null)
                    item.OtherName = item.OtherName.Replace("'", "").Replace("\"", "");
                cmd.Parameters["OtherName"].Value = item.OtherName; 

                if (item.Year.HasValue) cmd.Parameters["Years"].Value = item.Year.Value;
                else cmd.Parameters["Years"].Value = DBNull.Value;

                if (item.Developer != null)
                    item.Developer = item.Developer.Replace("'", "").Replace("\"", "");
                cmd.Parameters["Developer"].Value = item.Developer; 

                if (item.Players.HasValue) cmd.Parameters["Players"].Value = item.Players.Value;
                else cmd.Parameters["Players"].Value = DBNull.Value;

                //cmd.Parameters["Players"].Value = item.Players;
                cmd.Parameters["Genre"].Value = item.Genre;

                if (item.Annotation != null)
                    item.Annotation = item.Annotation.Replace("'", "").Replace("\"", "");
                cmd.Parameters["Annotation"].Value = item.Annotation;
                cmd.Parameters["Url"].Value = item.Url;
                cmd.Parameters["ImgUrl"].Value = item.ImgUrl;
                cmd.Parameters["DOWNLOADED"].Value = 0;
                cmd.ExecuteNonQuery();

            }


            fbTransaction.Commit();
            con.Close();

        }
    }
}
