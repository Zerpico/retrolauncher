﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RetroLauncher.View
{
    /// <summary>
    /// Логика взаимодействия для GameDetailControl.xaml
    /// </summary>
    public partial class GameDetailControl : UserControl
    {
        public GameDetailControl()
        {
            InitializeComponent();
            this.Opacity = 0;
        }

        private void ButtonNavigationBack_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as ViewModel.GameBaseViewModel).HideDetailPanel();
        }
    }
}
