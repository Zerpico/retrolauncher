﻿
using System.Collections.Generic;

namespace EmuParseLib
{
    public interface IParserSettings
    {
        string BaseUrl { get; set; }
        string[] Prefix { get; set; }       
    }
}
