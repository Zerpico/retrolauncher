﻿using RetroLauncher.Data;
using RetroLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetroLauncher.ViewModel.Wizard
{
    class WizardThreeViewModel : WizardBaseViewModel
    {
        public bool IsFinish = false;
        public event Action OnComplete;

        string AppDir = System.Environment.GetFolderPath(
        System.Environment.SpecialFolder.ApplicationData);

       // EmuParseLib.ParserWorker<IGame[]> parser;

        public WizardThreeViewModel()
        {
          
        }

        #region Свойства

        public override string Title
        {
            get
            {
                return "Идёт настройка";
            }
        }

        public override bool IsValid()
        {
            return true;
        }

        string textLog;
        public string TextLog
        {
            get { return textLog; }
            set { textLog = value; OnPropertyChanged("TextLog"); }
        }

        int progressMax;
        public int ProgressMax
        {
            get { return progressMax; }
            set { progressMax = value; OnPropertyChanged("ProgressMax"); }
        }

        int progressValue;
        public int ProgressValue
        {
            get { return progressValue; }
            set { progressValue = value; OnPropertyChanged("ProgressValue"); }
        }

        #endregion

        public void Start()
        {
            Worker();
        }

        //Начинаю всё парсить и скачивать
        public async void Worker()
        {
            //await Task.Factory.StartNew(() => { Storage.Load(); System.Threading.Thread.Sleep(100); Executer.OnUIThread(() => { TextLog = "Создан settings.xml"; }); });
            var emupars = new EmuParseLib.BaseParse.BaseSettings();
            ProgressMax = (emupars.Prefix.Count() * 2) + 3;

            Storage.Load();
            TextLog = "Создан settings.xml";

            ProgressValue = progressValue + 1;

            if (!System.IO.File.Exists(Paths.PathApp + "\\GAMEBASELIBRARY.GDB"))
                FBClient.CreateDatabase();


            var parser = new EmuParseLib.ParserWorker<IGame[]>(new EmuParseLib.BaseParse.BaseParse(), emupars);

            parser.OnComplete += Parser_OnComplete;
            parser.OnNewData += Parser_OnNewData;

            TextLog = textLog + "\nСкачивание базы";
            parser.Start();
        }

        //При добавлении новых данных об игры и в базу
        private void Parser_OnNewData(object arg1, IGame[] arg2)
        {
            var countGame = arg2.Count();
            TextLog = textLog + "\nОбновлено "+ countGame + " игр  для  " + arg2[0].Platform;
            ProgressValue = progressValue + 1;
            FBClient.Insert(arg2);
            ProgressValue = progressValue + 1;            
        }

        //при обновлении всей базы игр
        private void Parser_OnComplete(object obj)
        {
            TextLog = textLog + "\nБаза полностью обновлена!\n\nСкачивание эмулятора\n";           

            var parserString = new EmuParseLib.ParserWorker<string>(new EmuParseLib.EmuParse.EmuParse(), 
                new EmuParseLib.EmuParse.EmuSettings());

            parserString.OnNewData += ParserString_OnNewData;
            parserString.Start();
        }

        //когда узнали ссылку на эмулятор
        private void ParserString_OnNewData(object arg1, string arg2)
        {
            LoaderService loader = new LoaderService();
            loader.OnProgress += Loader_OnProgress;
            loader.OnComplete += Loader_OnComplete;
            loader.DownloadFile(arg2, AppDir + "\\RetroLauncher\\emulator.zip");
            ProgressValue = progressValue + 1;
        }

        //при скачивании архива эмулятора
        private void Loader_OnProgress(long arg1, long arg2, int arg3)
        {
            var logs = textLog.Split('\n');
            string newLog = "";
            for(int i=0; i< logs.Count()-1;i++)
            {
                newLog += logs[i] + "\n";
            }

            TextLog = newLog + "Скачано "+arg1/1024+" Kb из " + arg2/1024 + " Kb  ( "+arg3+"% )";
        }

        //когда скачали сам архив (и вообще всё скачали)
        private void Loader_OnComplete(bool obj)
        {
            ProgressValue = progressValue + 1;
            TextLog = textLog + "\nСкачано!";
            Utils.LoaderService.ExtractSevenZip(AppDir + "\\RetroLauncher\\emulator.zip", AppDir + "\\RetroLauncher\\emulator\\");
                       
            TextLog = textLog + "\n\nВсе настройки завершены";
            OnComplete?.Invoke();
        }
    }
}
