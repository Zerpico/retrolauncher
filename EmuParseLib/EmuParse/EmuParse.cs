﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Dom.Html;

namespace EmuParseLib.EmuParse
{
    /// <summary>
    /// Парсер сайта эмулятора, возвращает ссылку на архив
    /// </summary>
    public class EmuParse : IParser<string>
    {
        public string Parse(IHtmlDocument document, string BaseUrl)
        {
            //возвращаяемый результат
            var result = "";

            //отбираем элементы в ReleaseList для Windows
            var parseItem = document.QuerySelectorAll("ul").Where(item => item.ClassName == "ReleaseList" &&
                item.TextContent.Contains("Windows")).First();

            
            var items = parseItem.QuerySelectorAll("a").Where(item => item.TextContent.Contains("Windows")).First();
            
            //забираем ссылку на архив
            result = BaseUrl + items.Attributes["href"].Value;
            return result;
        }
    }
}
