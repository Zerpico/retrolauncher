﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetroLauncher.Utils
{
    public static class Paths
    {
        public static string AppDataPath
        {
            get
            {
                return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            }
        }

        /// <summary>
        /// RetroLauncher путь
        /// </summary>
        public static string PathApp
        {
            get { return AppDataPath + "\\RetroLauncher"; }
        }

        /// <summary>
        /// settings.xml
        /// </summary>
        public static string PathSettingsApp
        {
            get { return AppDataPath + "\\RetroLauncher\\settings.xml"; }
        }

        /// <summary>
        /// Пусть до папки с эмулятором
        /// </summary>
        public static string PathEmulator
        {
            get { return AppDataPath + "\\RetroLauncher\\emulator\\"; }
        }

        /// <summary>
        /// Путь до файла настроек эмулятора (mednafen.cfg)
        /// </summary>
        public static string PathEmulatorConfig
        {
            get { return AppDataPath + "\\RetroLauncher\\emulator\\mednafen.cfg"; }
        }

        /// <summary>
        /// Путь до папки с ромами игр
        /// </summary>
        public static string PathGames
        {
            get { return AppDataPath + "\\RetroLauncher\\Games\\"; }
        }

        
    }
}
