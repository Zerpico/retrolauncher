﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Navigation;

namespace RetroLauncher
{
    /// <summary>
    /// Логика взаимодействия для AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        Timer imageChangeTimer;
        public AboutWindow()
        {
            InitializeComponent();
            imageChangeTimer = new Timer(100);
            imageChangeTimer.Elapsed += imageChange_elapsed;
            this.Closed += AboutWindow_Closed;
            version_lbl.Content = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Loaded += AboutWindow_Loaded;
        }

        private void AboutWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Deactivated += (send, ev) =>
            {
                this.Close();
            };
        }

        private void AboutWindow_Closed(object sender, EventArgs e)
        {
            imageChangeTimer.Stop();
        }

        int picIndex = 1;

        private void imageChange_elapsed(object sender, ElapsedEventArgs e)
        {
            if (picIndex > 4)
                picIndex = 1;

            logoImg.Dispatcher.Invoke(() =>
            {
                logoImg.Source = new BitmapImage(new Uri($"pack://application:,,,/icon/images0000{picIndex}.png"));
            });


            picIndex++;
        }

        private void OnNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            imageChangeTimer.Start();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
