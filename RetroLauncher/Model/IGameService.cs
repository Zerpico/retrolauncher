﻿using RetroLauncher.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RetroLauncher.Model
{
    public interface IGameService
    {
        void GetData(Action<ObservableCollection<IGame>, Exception> callback);
        IGame[] FindGame(IGame findBook);
        (int, IGame[]) FindGame(int Count, int SkipCount, string name, string[] platform, string[] genre);
        void SetDownloaded(IGame game);
        void DownloadGame(IGame game);
        void StartGame(IGame game);
        event Action<int> OnProgressDownload;
        event Action OnComplete;

    }
}
